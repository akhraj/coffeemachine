package input;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.*;
import input.OrderBuilder;
import model.Ingredient;
import model.Order;
import model.inventory.InventoryRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class InputManager {

    public Integer getOutletCount(Map<String, Object> json) {
        Map<String, Object> machine = (Map<String, Object>) json.get("machine");
        Map<String,Object> outlets= (Map<String, Object>)machine.get("outlets");
        Integer count_n = (Integer)outlets.get("count_n");

        if(count_n<=0) throw new IllegalArgumentException("Count_n should be always greater than 0");
        return count_n;
    }

    public List<Order> getOrders(Map<String, Object> deserializedJson) {
        Map<String, Object> machine = (Map<String, Object>) deserializedJson.get("machine");
        Map<String, Object> ordersNode = (Map<String, Object>) machine.get("beverages");
        OrderBuilder orderBuilder = new OrderBuilder();
        return orderBuilder.buildOrders(ordersNode);
    }

    public List<InventoryRequest> getStock(Map<String, Object> deserializedJson) {
        List<InventoryRequest> inventoryRequests = new ArrayList<>();
        Map<String, Object> machine = (Map<String, Object>) deserializedJson.get("machine");
        if(machine.containsKey("total_items_quantity")) {
            Map<String, Object> ingredients = (Map<String, Object>)machine.get("total_items_quantity");
            for(Map.Entry<String, Object> ingredient : ingredients.entrySet()) {
                InventoryRequest ingredientRequest = InventoryRequest.builder()
                .ingredient(Ingredient.getIngredient(ingredient.getKey())).quantity((Integer) ingredient.getValue()).build();
                inventoryRequests.add(ingredientRequest);
            }
        }
        return inventoryRequests;
    }
}
