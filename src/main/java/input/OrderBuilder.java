package input;

import model.Ingredient;
import model.Order;
import model.inventory.InventoryRequest;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class OrderBuilder {

    private final int TIME = 1;


    public List<Order> buildOrders(Map<String, Object> ordersNode) {
        List<Order> orders = new ArrayList<>();
        for(Map.Entry<String, Object> menuItem : ordersNode.entrySet()) {
            Order order = buildOrder(menuItem);
            orders.add(order);
        }
        return orders;
    }

    private Order buildOrder(Map.Entry<String, Object> orderItem) {
        Map<String, Object> ingredients = (Map<String, Object>) orderItem.getValue();
        Set<InventoryRequest> inventoryRequests = new HashSet<>();

        for(Map.Entry<String, Object> ingredient : ingredients.entrySet()) {
            InventoryRequest inventoryRequest = InventoryRequest.builder()
                    .ingredient(Ingredient.getIngredient(ingredient.getKey()))
                    .quantity((Integer) ingredient.getValue())
                    .build();
            inventoryRequests.add(inventoryRequest);
        }

        return Order.builder()
                .inventoryRequests(inventoryRequests)
                .beverageName(orderItem.getKey())
                .orderId(Order.orderNumber.incrementAndGet())
                .preparationTime(Duration.of(TIME, ChronoUnit.SECONDS)).build();
    }

}
