package controllers;

import exceptions.InventoryStockException;
import lombok.AllArgsConstructor;
import model.Order;
import model.inventory.InventoryRequest;
import model.inventory.InventoryResponse;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mediator class bridges between the coffee machine and the inventory
 */
@AllArgsConstructor
public class Barista {

    InventoryManager inventoryManager;

    /**
     * Prepares the list of orders and submit to the coffeeMaker. This barista will get notified if the order failed or
     * succeeds
     * @param orders
     * @param coffeeMaker
     */
    public void prepareOrders(List<Order> orders, CoffeeMaker coffeeMaker) {
        for(Order order : orders)
            coffeeMaker.makeOrder(order, this);
    }

    /**
     * Atomically :
     *  check if all ingredients are available
     *  take ingredients from stock if all are available.
     *
     *  If all ingredients are not available report the inventory issue to caller.
     *
     * @param inventoryRequestSet
     * @exception InventoryStockException when some requests cannot be fulfilled.
     */
    public synchronized void fetchIngredients(Set<InventoryRequest> inventoryRequestSet) {

        Map<InventoryRequest, InventoryResponse> response = inventoryManager.checkInventory(inventoryRequestSet);
        boolean isOkay = response.values().stream()
                .filter(r -> r.getCode()!= InventoryResponse.Code.OK)
                .collect(Collectors.toList()).isEmpty();

        if(isOkay) {
            inventoryManager.removeFromInventory(inventoryRequestSet);
        } else {
            StringBuilder message = new StringBuilder();
            for(InventoryRequest inventoryRequest : inventoryRequestSet) {
                InventoryResponse inventoryResponse = response.get(inventoryRequest);
                if(inventoryResponse.getCode() == InventoryResponse.Code.INSUFFICIENT ||
                inventoryResponse.getCode() == InventoryResponse.Code.NOT_AVAILABLE) {
                    if(message.length()>0) message.append(",");
                    message.append(inventoryResponse.getIngredient().toString()+" is " + inventoryResponse.getCode().getMessage());
                }
            }
            throw new InventoryStockException(message.toString());
        }

    }

    /**
     * Audit the inventory
     */
    public void audit() {
        System.out.println("----------Inventory--------");
        List<InventoryResponse> inventoryResponses = this.inventoryManager.stockStatus();
        for(InventoryResponse inventoryResponse : inventoryResponses) {
            System.out.println(inventoryResponse.getIngredient() + "-" + inventoryResponse.getQuantity());
        }
        System.out.println("---------------------------");
    }

    /**
     * Restock
     * @param inventoryRequest
     */
    public void restock (InventoryRequest inventoryRequest) {
        this.inventoryManager.restock(inventoryRequest);
    }

    /**
     * Can notify anyone interested in this order, here its the console.
     * Synchronized as every task is being notified to the console. Not needed if the barista will notify different "customer" objects
     * @param outletTask
     */
    public synchronized void notifySuccess(OutletTask outletTask) {
        System.out.println("Order "+ outletTask.order.getOrderId()+"-"+ outletTask.order.getBeverageName() +" is prepared");
    }

    /**
     * Can notify anyone interested in this order, here its the console
     * Synchronized as every task is being notified to the console. Not needed if the barista will notify different "customer" objects
     * @param outletTask
     * @param message
     */
    public synchronized void notifyFailure(OutletTask outletTask, String message) {
        System.out.println("Cannot complete order "+ outletTask.order.getOrderId()+"-"+ outletTask.order.getBeverageName() + " because of "+message);
    }

}
