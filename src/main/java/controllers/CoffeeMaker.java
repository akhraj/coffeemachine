package controllers;

import lombok.Builder;
import lombok.Getter;
import model.Order;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CoffeeMaker {

    Integer numberOfOutlets;
    ExecutorService outlets;

    @Builder
    public CoffeeMaker(int numberOfOutlets) {
        this.numberOfOutlets = numberOfOutlets;
        outlets = Executors.newScheduledThreadPool(numberOfOutlets);
    }

    /**
     * Submits an order to an outlet
     * @param order
     * @param barista
     */
    void makeOrder(Order order, Barista barista) {
        OutletTask outletTask = OutletTask.builder()
                .order(order)
                .barista(barista)
                .build();
        outlets.submit(outletTask);
    }

    /**
     * Can be called to turn off the coffee machine
     */
    public void shutdownAfterCompletingOrders() throws InterruptedException {
        this.outlets.shutdown();
        this.outlets.awaitTermination(10000, TimeUnit.MILLISECONDS);
    }

}
