package controllers;

import model.Ingredient;
import model.inventory.InventoryRequest;
import model.inventory.InventoryResponse;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


public class InventoryManager {

    Map<Ingredient, AtomicInteger> store = new ConcurrentHashMap<>();

    /**
     * Checks inventory for the needed ingredients and returns a reponse
     * OK -- have the required amount of ingredient
     * INSUFFICIENT -- Have some in stock but not entire amount that is required
     * NOT_AVAILABLE -- Emptied out by previous orders or never was in stock.
     * @param inventoryRequests
     * @return
     */
    Map<InventoryRequest, InventoryResponse> checkInventory(Set<InventoryRequest> inventoryRequests) {
        Map<InventoryRequest, InventoryResponse> responses = new HashMap<>();
        InventoryResponse response = null;
        for (InventoryRequest inventoryRequest : inventoryRequests) {
            Ingredient ingredient = inventoryRequest.getIngredient();
            int requiredQuantity = inventoryRequest.getQuantity();
            if (store.containsKey(ingredient)) {
                AtomicInteger quantity = store.get(ingredient);
                if (quantity.get() >= requiredQuantity) {//got the full amount
                    response = InventoryResponse.builder()
                            .ingredient(ingredient)
                            .quantity(requiredQuantity)
                            .code(InventoryResponse.Code.OK)
                            .build();
                } else {
                    if (quantity.get() > 0)
                        response = InventoryResponse.builder()
                                .ingredient(ingredient)
                                .quantity(quantity.get())
                                .code(InventoryResponse.Code.INSUFFICIENT)
                                .build();
                    else
                        response = InventoryResponse.builder()
                                .ingredient(ingredient)
                                .quantity(quantity.get())
                                .code(InventoryResponse.Code.NOT_AVAILABLE)
                                .build();
                }
            } else {
                response = InventoryResponse.builder()
                        .ingredient(ingredient)
                        .quantity(0)
                        .code(InventoryResponse.Code.NOT_AVAILABLE)
                        .build();
            }
            responses.put(inventoryRequest, response);
        }
        return responses;
    }

    /**
     * Consumes the ingredients
     * @param inventoryRequests
     */
    void removeFromInventory(Set<InventoryRequest> inventoryRequests) {
        for (InventoryRequest inventoryRequest : inventoryRequests) {
            Ingredient ingredient = inventoryRequest.getIngredient();
            int requiredQuantity = inventoryRequest.getQuantity();
            if (store.containsKey(ingredient)) {
                AtomicInteger quantity = store.get(ingredient);
                if (quantity.get() < requiredQuantity)
                    throw new UnsupportedOperationException("Inventory does not have " + requiredQuantity + " of " + ingredient);
                quantity.addAndGet(-requiredQuantity);
            } else
                throw new IllegalArgumentException("Unknown Ingredient : " + ingredient);
        }
    }

    /**
     * Returns current state of inventory
     * @return
     */
    List<InventoryResponse> stockStatus() {
        List<InventoryResponse> inventoryResponses = new ArrayList<>();
        for (Map.Entry<Ingredient, AtomicInteger> entry : store.entrySet()) {
            InventoryResponse inventoryResponse = InventoryResponse.builder().ingredient(entry.getKey())
                    .quantity(entry.getValue().get())
                    .code(InventoryResponse.Code.OK)
                    .build();
            inventoryResponses.add(inventoryResponse);
        }
        return inventoryResponses;
    }

    /**
     * Restocks inventory with the given quantity of ingredient
     * @param inventoryRequest
     */
    void restock(InventoryRequest inventoryRequest) {
        Ingredient ingredient = inventoryRequest.getIngredient();
        Integer quantity = inventoryRequest.getQuantity();
        AtomicInteger currentQuantity = store.getOrDefault(ingredient, new AtomicInteger(0));
        currentQuantity.addAndGet(quantity);
        store.put(ingredient, currentQuantity);
    }

}
