package controllers;

import exceptions.InventoryStockException;
import lombok.Builder;
import model.Order;

@Builder
public class OutletTask implements Runnable{

    Order order;
    Barista barista;

    /**
     * Preps the order
     */
    @Override
    public void run() {
        try {
            System.out.println("Preparing "+this.order.getOrderId()+"-"+ order.getBeverageName());
            barista.fetchIngredients(order.getInventoryRequests());
            prepareOrder();
            barista.notifySuccess(this);
        } catch (InventoryStockException ex) {
             String message = "following inventory issues: "+ex.getMessage();
             barista.notifyFailure(this, message);
        } catch (Exception ex) {
            barista.notifyFailure(this, "Coffee machine failure "+ex.getMessage());
        }

    }

    private void prepareOrder() throws InterruptedException {
        Thread.sleep(order.getPreparationTime().toMillis());
    }

    public String getOrderBeverage() {
        return this.order.getBeverageName();
    }
}
