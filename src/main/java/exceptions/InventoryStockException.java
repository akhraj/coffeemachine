package exceptions;

public class InventoryStockException extends RuntimeException{
    public InventoryStockException(String message){
        super(message);
    }
}
