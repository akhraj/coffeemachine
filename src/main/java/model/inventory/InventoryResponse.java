package model.inventory;

import lombok.Builder;
import lombok.Getter;
import model.Ingredient;

@Builder
@Getter
public class InventoryResponse {

    private Ingredient ingredient;
    private int quantity;
    private Code code;

    public enum Code {

        OK("Ok"),
        INSUFFICIENT("Insufficient"),
        NOT_AVAILABLE("Not Available");

        @Getter
        private String message;

        Code(String message) {
            this.message = message;
        }
    }

}
