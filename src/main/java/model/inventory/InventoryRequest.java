package model.inventory;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import model.Ingredient;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
public class InventoryRequest {

    private Ingredient ingredient;
    private Integer quantity;

    public static InventoryRequestBuilder builder() {
        return new InventoryRequestBuilder();
    }

    public static class InventoryRequestBuilder {
        private Ingredient ingredient;
        private Integer quantity;

        InventoryRequestBuilder() {
        }

        public InventoryRequestBuilder ingredient(Ingredient ingredient) {
            this.ingredient = ingredient;
            return this;
        }

        public InventoryRequestBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            if(quantity<0) throw new IllegalArgumentException("Quantity of ingredients should be greater than or equal to 0");
            return this;
        }

        public InventoryRequest build() {
            return new InventoryRequest(ingredient, quantity);
        }

        public String toString() {
            return "InventoryRequest.InventoryRequestBuilder(ingredient=" + this.ingredient + ", quantity=" + this.quantity + ")";
        }
    }
}
