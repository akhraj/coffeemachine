package model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import model.inventory.InventoryRequest;

import java.time.Duration;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Builder
public class Order {

    public static AtomicInteger orderNumber = new AtomicInteger(0);

    int orderId;
    private final String beverageName;
    private final Set<InventoryRequest> inventoryRequests;
    private final Duration preparationTime;

}
