package model;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Ingredient {
    private String name;

    private Ingredient(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Ingredient getIngredient(String name) {
        return new Ingredient(name);
    }
}
