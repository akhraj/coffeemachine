package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.Barista;
import controllers.CoffeeMaker;
import controllers.InventoryManager;
import controllers.OutletTask;
import input.InputManager;
import model.Ingredient;
import model.Order;
import model.inventory.InventoryRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Or;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CoffeeMakerTest {

    Map<String, Object> deserializedJson;
    InputManager inputManager = new InputManager();
    InventoryManager inventoryManager = new InventoryManager();


    @Before
    public void setup() throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        String json = readJson();
        deserializedJson = objectMapper.readValue(json, Map.class);

    }

    @Test
    public void testOrderPreparation() throws InterruptedException {
        int count_n = inputManager.getOutletCount(deserializedJson);
        List<Order> orders = inputManager.getOrders(deserializedJson);
        List<InventoryRequest> restocks = inputManager.getStock(deserializedJson);
        CoffeeMaker coffeeMaker = new CoffeeMaker(count_n);
        Barista barista = Mockito.spy(new Barista(inventoryManager));
        for(InventoryRequest inventoryRequest: restocks) barista.restock(inventoryRequest);

        barista.prepareOrders(orders, coffeeMaker);
        coffeeMaker.shutdownAfterCompletingOrders();
        barista.audit();

        Mockito.verify(barista, times(2)).notifySuccess(Matchers.argThat(new ArgumentMatcher<OutletTask>() {
            @Override
            public boolean matches(Object o) {
                if(o instanceof OutletTask) {
                    String expectedToSucceed = "hot_coffee|hot_tea|black_tea";
                    OutletTask outletTask = (OutletTask) o;
                    return expectedToSucceed.contains(outletTask.getOrderBeverage());
                }
                return false;
            }
        }));
        Mockito.verify(barista, times(2)).notifyFailure(Mockito.any(OutletTask.class), Mockito.any(String.class));
    }

    @Test
    public void testRestocking() throws InterruptedException {

        String greenTeaError = "following inventory issues: green_mixture is Not Available,sugar_syrup is Insufficient";

        int count_n = inputManager.getOutletCount(deserializedJson);
        List<Order> defaultOrders = inputManager.getOrders(deserializedJson);
        List<Order> orders = List.of(defaultOrders.get(0), defaultOrders.get(1), defaultOrders.get(3)); //hot tea, hot coffee, green tea

        List<InventoryRequest> restocks = inputManager.getStock(deserializedJson);
        CoffeeMaker coffeeMaker = new CoffeeMaker(count_n);
        Barista barista = Mockito.spy(new Barista(inventoryManager));
        for(InventoryRequest inventoryRequest: restocks) barista.restock(inventoryRequest);

        barista.prepareOrders(orders, coffeeMaker);
        Thread.sleep(2000);
        //green tea failed, restocking
        InventoryRequest greenPowder = InventoryRequest.builder()
                .ingredient(Ingredient.getIngredient("green_mixture"))
                .quantity(30).build();
        InventoryRequest sugarSyrup = InventoryRequest.builder()
                .ingredient(Ingredient.getIngredient("sugar_syrup"))
                .quantity(30).build();

        barista.restock(greenPowder);
        barista.restock(sugarSyrup);

        //try green tea again
        barista.prepareOrders(List.of(defaultOrders.get(3)), coffeeMaker);
        Thread.sleep(2000);
        coffeeMaker.shutdownAfterCompletingOrders();
        barista.audit();

        //Green tea failed once since green_mixture is not available
        Mockito.verify(barista, times(1)).notifyFailure(any(), Matchers.contains("green_mixture is Not Available"));
        //Green tea succeeds after restocking
        Mockito.verify(barista, times(1)).notifySuccess(Matchers.argThat(new ArgumentMatcher<OutletTask>() {
            @Override
            public boolean matches(Object o) {
                if(o instanceof OutletTask) {
                    OutletTask outletTask = (OutletTask) o;
                    return outletTask.getOrderBeverage().equals("green_tea");
                }
                return false;
            }
        }));
    }


    private static String readJson() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("state.json");
        InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        for (String line; (line = reader.readLine()) != null;) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

}
